package com.codenjoy.dojo.snake.client;

import java.util.Objects;

public class MyPoint {

    public final int x;
    public final int y;

    private MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static MyPoint of(int x, int y) {
        return new MyPoint(x, y);
    }

    public MyPoint move(int dx, int dy) {
        return new MyPoint(x + dx, y + dy);
    }

    @Override
    public String toString() {
        return String.format("[ %2d, %2d ]", x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyPoint myPoint = (MyPoint) o;
        return x == myPoint.x && y == myPoint.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
