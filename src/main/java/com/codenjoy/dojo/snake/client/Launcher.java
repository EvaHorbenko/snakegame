package com.codenjoy.dojo.snake.client;
import com.codenjoy.dojo.client.WebSocketRunner;

public class Launcher {

    public static void main(String[] args) {
        String url = "http://64.226.126.93/codenjoy-contest/board/player/u7a1yfrhxpvki0mek55c?code=9158380908523034060";
        MySolver solver = new MySolver();
        Board board = new Board();
        WebSocketRunner.runClient(url, solver, board);
    }

}
