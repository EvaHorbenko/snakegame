package com.codenjoy.dojo.snake.client;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lee {
       private final int EMPTY = 0;
       private final int OBSTACLE = -1;
       private final int START = 1;
       private final int width;
       private final int height;
       private final int[][] board;

       private Lee(int width, int height) {
              this.width = width;
              this.height = height;
              this.board = new int[height][width];
       }
       public static Lee of (int width, int height) {
              return new Lee(width, height);
       }

       private int get(int x, int y) {
              return board[y][x];
       }
       private int get(MyPoint point) {
              return get(point.x, point.y);
       }
       private void set(int x, int y, int value) {
              board[y][x] = value;
       }
       private void set(MyPoint point, int value) {
              board[point.y][point.x] = value;
       }
       private boolean isOnBoard(MyPoint point) {
              return point.x >= 0 && point.x < width &&
                      point.y >= 0 && point.y < height;
       }
       private boolean isUnvisited(MyPoint point) {
              return get(point) == EMPTY;
       }
       private Supplier<Stream<MyPoint>> deltas() {
              return () -> Stream.of(
                      MyPoint.of(1, 0),
                      MyPoint.of(-1, 0),
                      MyPoint.of(0, 1),
                      MyPoint.of(0, -1)
              );
       }
       private Stream<MyPoint> neighbours(MyPoint point) {
              return deltas().get()
                      .map(d -> point.move(d.x, d.y))
                      .filter(this::isOnBoard);
       }
       private Stream<MyPoint> neighboursUnvisited(MyPoint point) {
              return neighbours(point)
                      .filter(this::isUnvisited);
       }
       private List<MyPoint> neighboursByValue(MyPoint point, int value) {
              return neighbours(point)
                      .filter(p -> get(p) == value)
                      .toList();
       }
       private void initializeBoard (List<MyPoint> obstacles) {
              obstacles.forEach(o -> set(o, OBSTACLE));
       }
       private void clearBoard () {
              for (int i = 0; i < board.length; i++) {
                     for (int j = 0; j < board[i].length; j++) {
                            set(i, j, EMPTY);
                     }
              }
       }

       public Optional<List<MyPoint>> trace(MyPoint start, MyPoint finish, List<MyPoint> obstacles) {
              clearBoard();
              initializeBoard(obstacles);
              int[] counter = {START}; // HEAP due to lambda
              set(start, counter[0]);
              counter[0]++;
              boolean found = false;
              // 2. fill the board
              for (Set<MyPoint> curr = new HashSet<>(Set.of(start)); !(found || curr.isEmpty()); counter[0]++) {
                     Set<MyPoint> next = curr.stream()
                             .flatMap(this::neighboursUnvisited)
                             .collect(Collectors.toSet());

                     next.forEach(p -> set(p, counter[0]));
                     found = next.contains(finish);
                     curr = next;
              }
              // 3. backtrace (reconstruct the path)
              if (!found) return Optional.empty();
              LinkedList<MyPoint> path = new LinkedList<>();
              path.add(finish);
              counter[0]--;

              MyPoint curr = finish;
              while (counter[0] > START) {
                     counter[0]--;
                     MyPoint prev = neighboursByValue(curr, counter[0]).get(0);
                     if (prev == start) break;
                     path.addFirst(prev);
                     curr = prev;
              }

              return Optional.of(path);
       }

       public String cellFormatted(MyPoint p, List<MyPoint> path) {
              int value = get(p);
              String valueF = String.format("%3d", value);

              if (value == OBSTACLE) {
                     return " XX";
              }

              return valueF;//" --";
       }

       public String boardFormatted(List<MyPoint> path) {
              return IntStream.range(0, height).mapToObj(y ->
                              IntStream.range(0, width)
                                      .mapToObj(x -> MyPoint.of(x, y))
                                      .map(p -> cellFormatted(p, path))
                                      .collect(Collectors.joining())
                      )
                      .collect(Collectors.joining("\n"));
       }

       @Override
       public String toString() {
              return boardFormatted(Collections.emptyList());
       }

}
