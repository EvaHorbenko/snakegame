package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;

import java.util.*;

public class MySolver implements Solver<Board> {
    private Board board;

    public Direction solveIt(Board board) {
        this.board = board;
        int[] boardSize = size();
        Lee lee = Lee.of(boardSize[0], boardSize[1]);

        List<Point> snake = board.getSnake();
        MyPoint head = pointFrom(board.getHead());
        MyPoint tail = pointFrom(snake.get(snake.size() - 1));
        MyPoint apple = pointFrom(board.getApples().get(0));
        MyPoint stone = pointFrom(board.getStones().get(0));

        List<MyPoint> obstacles = board.getBarriers().stream().map(this::pointFrom).toList();
        List<MyPoint> obstaclesExcludeStone = obstaclesExclude(board.getStones().get(0));
//                board.getBarriers().stream()
//                .filter(p -> !p.equals(board.getStones().get(0)))
//                .map(this::pointFrom).toList();
        List<MyPoint> obstaclesExcludeTale = obstaclesExclude(snake.get(snake.size() - 1));
//                board.getBarriers().stream()
//                .filter(p -> !pointFrom(p).equals(tail))
//                .map(this::pointFrom).toList();

        int maxSnakeLength = 36;
//        int maxSnakeLength = (int) Math.floor(((double) (boardSize[0] - 2 + boardSize[1] - 2) / 2) * 2.5); // 32
//        int maxSnakeLength = (boardSize[0] - 2 + boardSize[1] - 2) -1; // 25

        Optional<List<MyPoint>> appleTrace = lee.trace(head, apple, obstacles);
        Optional<List<MyPoint>> stoneTrace = lee.trace(head, stone, obstaclesExcludeStone);
        Optional<List<MyPoint>> tailTrace = lee.trace(head, tail, obstaclesExcludeTale);
        Optional<List<MyPoint>> trace = Optional.empty();

        if (appleTrace.isPresent()) {
            trace = appleTrace;
        }
        if (appleTrace.isEmpty() && stoneTrace.isPresent()) {
            trace = stoneTrace;
        }
        if (snake.size() >= maxSnakeLength && stoneTrace.isPresent()) {
            trace = stoneTrace;
        }
        if (
                appleTrace.isPresent() && stoneTrace.isPresent()
                && snake.size() > maxSnakeLength - 3
                && appleTrace.get().size() - stoneTrace.get().size() > 7
        )  {
            trace = stoneTrace;
        }
        if (
                appleTrace.isPresent() && stoneTrace.isPresent()
                && snake.size() < maxSnakeLength + 3
                && stoneTrace.get().size() - appleTrace.get().size() > 7
        )  {
            trace = appleTrace;
        }
        if (appleTrace.isEmpty() && stoneTrace.isEmpty() && tailTrace.isPresent()) {
            trace = tailTrace;
        }
        if (appleTrace.isEmpty() && stoneTrace.isEmpty() && tailTrace.isEmpty()) {
            Optional<List<MyPoint>> lastSeenTailPointTrace = Optional.empty();
            int tailCounter = snake.size() - 1;
            while (lastSeenTailPointTrace.isEmpty() || tailCounter == 0) {
                Point tailPoint = snake.get(tailCounter);
                lastSeenTailPointTrace = lee.trace(head, pointFrom(tailPoint), obstaclesExclude(tailPoint));
                tailCounter--;
            }
            if (lastSeenTailPointTrace.isEmpty()) return killSelf();

            Point lastSeenTailPoint = snake.get(tailCounter);
            int toTailEnd = snake.size() - tailCounter;
            int toHead = lastSeenTailPointTrace.get().size();

            if (toTailEnd > toHead - 1) return killSelf();
            trace = lee.trace(head, pointFrom(lastSeenTailPoint), obstaclesExclude(lastSeenTailPoint));
        }

        if (trace.isEmpty()) return killSelf();
        Optional<Direction> direction = directionFromTrace(head, trace.get().get(1));
        return direction.orElseGet(this::killSelf);
    }

    private List<MyPoint> obstaclesExclude(Point point) {
        return board.getBarriers().stream()
                .filter(p -> !p.equals(point))
                .map(this::pointFrom).toList();
    }

    private Optional<Direction> directionFromTrace(MyPoint head, MyPoint next) {
//        System.out.println("=".repeat(50));
//        System.out.printf(">>> :: head: %s, next: %s", head, next);
//        System.out.println("=".repeat(50));
        if (head.x > next.x) return Optional.of(Direction.LEFT);
        if (head.x < next.x) return Optional.of(Direction.RIGHT);
        if (head.y > next.y) return Optional.of(Direction.DOWN);
        if (head.y < next.y) return Optional.of(Direction.UP);
        return Optional.empty();
    }
    private Direction killSelf() {
        return board.getSnakeDirection().inverted();
    }
    private MyPoint pointFrom(Point point) {
        return MyPoint.of(point.getX(), point.getY());
    }

    private int[] size() {
        List<Point> walls = board.getWalls();
        int[] size = {0, 0};
        for (Point wall : walls) {
            if (wall.getX() > size[0]) {
                size[0] = wall.getX() + 2;
            }
            if (wall.getY() > size[1]) {
                size[1] = wall.getY() + 2;
            }
        }
        return size;
    }


    @Override
    public String get(Board board) {
        return solveIt(board).toString();
    }
}
